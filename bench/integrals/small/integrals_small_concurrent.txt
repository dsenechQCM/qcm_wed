#pyqcm version 2.2.4
#Total runtime on a 2 x AMD EPYC 7643 @ 2.3 GHz (48 cores each, no HT) cp3702
#Concurent run with OMP_NUM_THREADS=4 or 6
#FLEXIBLAS=OPENBLAS, OPENBLAS_NUM_THREADS=1
Concurrent,4T,6T,8T
1,17.8,12.9,10.0
2,17.9,12.9,10.4
4,17.9,12.5,10.3
6,18.1,13.5,11.3
8,18.1,14.7,21.9
9,18.9,,
10,19.4,25.5,
11,20.2,,
12,20.7,26.3,35.3
13,26.0,,
14,30.3,,
15,33.2,,
16,34.9,32.2,
24,39.0,,
