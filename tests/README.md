# <div align="center"> Testing `pyqcm` </div>

There are two main ways of using the tests contained within `qcm_wed/tests`:

1. *All tests* included can be run using `test_all.py` which will output test results to the terminal at the very end with the format expected for `unittest`. Any output from the tests is stowed within `tests/test_outputs`.

2. *Individual tests* can be run directly from the directory `tests/test_files`. Note : not all tests have an individual "test-like" ouput.