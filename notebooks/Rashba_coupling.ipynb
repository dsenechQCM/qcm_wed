{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Rashba coupling\n",
    "\n",
    "Construct a one-dimensional Hubbard model with the following non-interacting Hamiltonian, here expressed in $k$-space:\n",
    "$$\n",
    "    H_0 = \\sum_k \\mathcal{H}_k  ~~~~~ \\mathcal{H}_k = -2t\\cos k\\;c^\\dagger_k c_k - 2m \\sin k\\;c^\\dagger_k \\sigma_x c_k\n",
    "$$\n",
    "where $c_k$ is a two-component annihilation operator in spin space : $(c_{k,\\uparrow}, c_{k,\\downarrow})$ and $\\sigma_x$ is a Pauli matrix.\n",
    "Plot the non-interacting $\\left(U=0\\right)$ dispersion of this model as a function of $k$ in the interval $[-\\pi,\\pi]$ for $t=1$ and $m=0.2$ and check that it agrees with your analytical computation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyqcm\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# declare a cluster model of 4 sites, named 'clus'\n",
    "CM = pyqcm.cluster_model( 4)\n",
    "\n",
    "# define a physical cluster based on that model, with base position (0,0,0) and site positions\n",
    "clus = pyqcm.cluster(CM, ((0,0,0), (1,0,0), (2,0,0),(3,0,0))) \n",
    "\n",
    "# define a lattice model named '1D_4' made of the cluster(s) clus and superlattice vector (4,0,0)\n",
    "model = pyqcm.lattice_model('1D_4', clus, ((4,0,0),))\n",
    "\n",
    "# define a few operators in this model\n",
    "model.interaction_operator('U')\n",
    "model.hopping_operator('t', (1,0,0), -1)\n",
    "# Defining the one-body Rashba coupling term\n",
    "# tau=2 ---> imaginary contribution, ensures sin(k), sigma=1 ---> spin flip\n",
    "model.hopping_operator(\"m\", (1,0,0), -1, tau=2, sigma=1) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note that spin is not conserved due to the Rashba term\n",
    "model.set_target_sectors('R0:N4') \n",
    "model.set_parameters(\"\"\"\n",
    "    t=1\n",
    "    m=0.4\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plotting the spectral function\n",
    "I = pyqcm.model_instance(model)\n",
    "I.spectral_function(wmax=2.5, path=\"line\", nk=64, orb=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interpretation\n",
    "\n",
    "As we can see, the addition of the Rashba coupling term to the kinetic hamilonian creates two energy levels instead of one.\n",
    "\n",
    "### \"Manual\" energy eigenvalues\n",
    "\n",
    "First, consider the following definition for the hamiltonian with Rashba coupling:\n",
    "$$\n",
    "    H_0 = \\sum_{k}\\mathcal{H}_k ~~,~~ \\mathcal{H}_k = -2t\\operatorname{cos}(k)c_k^\\dagger c_k -2t\\operatorname{sin}(k)c_k^\\dagger\\sigma_x c_k~~,~~ \n",
    "    c_k\\equiv \\begin{pmatrix}\n",
    "        c_{k\\uparrow} \\\\\n",
    "        c_{k\\downarrow}\n",
    "    \\end{pmatrix} \\Rightarrow \n",
    "    c_k^\\dagger = \\begin{pmatrix}\n",
    "        c_{k\\uparrow}^\\dagger &\n",
    "        c_{k\\downarrow}^\\dagger\n",
    "    \\end{pmatrix}\n",
    "$$\n",
    "With these definitions, \n",
    "$$\n",
    "    \\mathcal{H}_k = -2t\\operatorname{cos}(k)\\left[ c_{k\\uparrow}^\\dagger c_{k\\uparrow} + c_{k\\downarrow}^\\dagger c_{k\\downarrow} \\right] -2t\\operatorname{sin}(k)\\left[ c_{k\\uparrow}^\\dagger c_{k\\downarrow} + c_{k\\downarrow}^\\dagger c_{k\\uparrow} \\right]\n",
    "$$\n",
    "Or more interestingly,\n",
    "$$\n",
    "    \\mathcal{H}_k = c_k^\\dagger\n",
    "    \\underbrace{\\begin{pmatrix}\n",
    "        -2t\\operatorname{cos}(k) & -2m\\operatorname{sin}(k) \\\\\n",
    "        -2m\\operatorname{sin}(k) & -2t\\operatorname{cos}(k) \n",
    "    \\end{pmatrix}}_{M}\n",
    "    c_k\n",
    "$$\n",
    "Finding the eigenvalues of $M$ is trivial:\n",
    "$$\n",
    "    \\varepsilon(k) = -2t\\operatorname{cos}(k) \\pm 2m\\operatorname{sin}(k) = 2\\left[ -t\\operatorname{cos}(k) \\pm m\\operatorname{sin}(k) \\right]\n",
    "$$\n",
    "Plotting these should yield the same result as the numerical computations above.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# creating a grid of wavevectors in the Brillouin zone\n",
    "k_grid = np.linspace(-np.pi, np.pi, 150)\n",
    "\n",
    "t=1 # value of the hopping operator\n",
    "m=0.2 # value of the coupling term\n",
    "\n",
    "\n",
    "# Defining the energy eigenvalues found by hand\n",
    "def eig_1(k, t, m):\n",
    "    return -2*(t*np.cos(k) + m*np.sin(k))\n",
    "\n",
    "def eig_2(k, t, m):\n",
    "    return -2*(t*np.cos(k) - m*np.sin(k))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plotting the analytic dispersion curve\n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "ax.plot(k_grid, eig_1(k_grid, t, m), label=\"$\\epsilon_1(k)$\")\n",
    "ax.plot(k_grid, eig_2(k_grid, t, m), label=\"$\\epsilon_2(k)$\")\n",
    "\n",
    "ax.set_xlabel(\"k\")\n",
    "ax.set_ylabel(\"$\\epsilon(k)$\")\n",
    "\n",
    "ax.set_xticks((-np.pi,0,np.pi))\n",
    "ax.set_xticklabels((\"$-\\pi$\",\"$0$\",\"$-\\pi$\"))\n",
    "\n",
    "ax.legend()\n",
    "\n",
    "fig.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "q_env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "9181c3fee36b2a40316c7a7a7190b5ebac7b971b62240dc3c988739896c48274"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
