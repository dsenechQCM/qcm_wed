{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Mott transition in a Weyl semimetal\n",
    "A simple model of a Weyl semimetal is defined by the following Hamiltonian:\n",
    "\n",
    "$$ H = H_0 + U\\sum_{i} n_{i\\uparrow}n_{i_\\downarrow} $$\n",
    "\n",
    "where\n",
    "\n",
    "$$ H_0 = \\sum_{\\mathbf{k}} c_\\mathbf{k}^\\dagger \\mathcal{H}_\\mathbf{k} c_\\mathbf{k} \\qquad\\qquad\n",
    "c_\\mathbf{k}=(c_{\\mathbf{k}\\uparrow}, c_{\\mathbf{k}\\downarrow}) $$\n",
    "\n",
    "with the $k$-dependent matrix \n",
    "\n",
    "$$ \\mathcal{H}_\\mathbf{k} =  \\left\\{ 2t(\\cos k_x - \\cos k_0) + m(2-\\cos k_y -\\cos k_z)\\right\\}\\sigma_x\n",
    "+ 2t\\sin k_y \\sigma_y + 2t\\sin k_z \\sigma_z $$\n",
    "\n",
    "where $\\sigma_{x,y,z}$ are the Pauli matrices.\n",
    "We will set $m=\\frac32$ and $k_0=3\\pi/8$.\n",
    "See Witczak-Krempa *et al.*, Phys. Rev. Lett. **113**, 136402 (2014).\n",
    "\n",
    "1. Construct this model, with the operators $t$, $m$ and $U$.\n",
    "2. Plot the Fermi surface at $U=0$ using `spectral.mdc()` and identify the Weyl nodes along the $k_x$ axis.\n",
    "3. Using CPT, show how the position of the Weyl node changes for $U=6$ compared with $U=0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyqcm\n",
    "import numpy as np\n",
    "\n",
    "# declare a cluster model of 8 sites, named 'clus'\n",
    "CM = pyqcm.cluster_model( 8)\n",
    "\n",
    "# define a physical cluster based on that model, with base position (0,0,0) and site positions\n",
    "clus = pyqcm.cluster(CM, ((0,0,0), (1,0,0), (0,1,0), (1,1,0), (0,0,1), (1,0,1), (0,1,1), (1,1,1))) \n",
    "\n",
    "# define a lattice model named '2x2' made of the cluster(s) clus and superlattice vectors (2,0,0) & (0,2,0)\n",
    "model = pyqcm.lattice_model('2x2x2', clus, ((2,0,0), (0,2,0), (0,0,2)))\n",
    "\n",
    "# define a few operators in this model\n",
    "model.interaction_operator('U')\n",
    "# Assembling the parts of operator `t`\n",
    "model.hopping_operator(\"t\", (1,0,0), 1, tau=1, sigma=1)\n",
    "model.hopping_operator(\"t\", (0,0,0), -np.cos(3*np.pi/8), tau=0, sigma=1)\n",
    "model.hopping_operator(\"t\", (0,1,0), 1, tau=2, sigma=2)\n",
    "model.hopping_operator(\"t\", (0,0,1), 1, tau=2, sigma=3)\n",
    "\n",
    "# Assembling the parts of operator `m`\n",
    "model.hopping_operator(\"m\", (0,0,0), 1, tau=0, sigma=1)\n",
    "model.hopping_operator(\"m\", (0,1,0), -0.5, tau=1, sigma=1)\n",
    "model.hopping_operator(\"m\", (0,0,1), -0.5, tau=1, sigma=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Defining a model at half-filling \n",
    "model.set_target_sectors([\"R0:N8\"])\n",
    "model.set_parameters(\"\"\"\n",
    "    U = 4\n",
    "    mu = 0.5*U\n",
    "    t=1\n",
    "    m=1.5\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"plotting different Fermi surface plots at U=0...\")\n",
    "model.set_parameter(\"U\", 1e-9)\n",
    "I = pyqcm.model_instance(model)\n",
    "I.mdc(plane=\"xy\")\n",
    "I.mdc(plane=\"yz\")\n",
    "\n",
    "print(\"plotting different Fermi surface plots at U=6...\")\n",
    "model.set_parameter(\"U\", 6)\n",
    "I = pyqcm.model_instance(model)\n",
    "I.mdc(plane=\"xy\")\n",
    "I.mdc(plane=\"yz\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"plotting the spectral function along a high-symmetry path at U=0...\")\n",
    "model.set_parameter(\"U\", 1e-9)\n",
    "I = pyqcm.model_instance(model)\n",
    "I.spectral_function(path=\"cubic\")\n",
    "\n",
    "print(\"plotting the spectral function along a high-symmetry path at U=0...\")\n",
    "model.set_parameter(\"U\", 6)\n",
    "I = pyqcm.model_instance(model)\n",
    "I.spectral_function(path=\"cubic\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example uses of the Berry phase functions\n",
    "An array of functions are available in the submodule `pyqcm.berry` to display the topological properties of the model.\n",
    "\n",
    "#### Berry_curvature()\n",
    "`Berry_curvature(...)` draws a 2D density plot of the Berry curvature $\\mathcal{B}(k_1,k_2)$ as a function of wavevector, by default on a square grid going from $-\\pi$ to $\\pi$ in each direction. It is possible to modify the range, the orientation of the plane ($k_xk_y$, $k_xk_z$ or $k_yk_z$), the value of $k_3$ (the level of the plane), the periodization scheme, etc. (see documentation). The Berry curvature is computed by adding the zero-frequency self-energy (real part) to the Hamiltonian and treated the result as a one-body Hamiltonian, as explained in  Z. Wang and S.C. Zhang, Physical Review X **2**, 031008 (2012).\n",
    "The numerical computation itself is inspired by Takahiro Fukui, Yasuhiro Hatsugai, and Hiroshi Suzuki, Journal of the Physical Society of Japan **74**, 1674 (2005), with an important caveat: the possibility of grid refining (optional boolean parameter `rec`) if the if connexion is too large, which could be the sign of missed *twists* between grid points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"plotting the Berry curvature...\")\n",
    "I.Berry_curvature(nk=100, eta=0.0, period='G', range=None, label=0, orb=1, subdivide=False, plane='xy', k_perp=0.1, file=None, plt_ax=None)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Berry_flux()\n",
    "`Berry_flux(k0, R, nk=40, plane='xy', label=0)` computes the flux of the Berry curvature through a polygonal circle of `nk` sides and radius `R` centered at wavevector `k0` in the plane `plane`. The computation is made by integrating the Berry connexion along the circle. This function only returns a value, it does not create a plot. However, `Berry_flux_map()` does just that. This is in fact a different way of computing the Berry curvature, in principle more time consuming than `Berry_curvature(...)`, but independent of the computational method described in Fukui et al."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"computing the flux of the Berry curvature through loops located at different values of kx...\")\n",
    "for kx in np.arange(0,1,0.1):\n",
    "    print('kx/pi = {:.2f} : Berry flux = {:.2f}'.format(kx, 1e-7+I.Berry_flux([kx,0,0], 0.05, nk=10, plane='xy')))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.set_parameter(\"U\", 0)\n",
    "I = pyqcm.model_instance(model)\n",
    "I.Berry_flux_map(nk=50, plane='z', dir='z', k_perp=0.01, label=0, npoints=8, radius=None, file=None, plt_ax=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Berry_field_map()\n",
    "`Berry_field_map()` Does the same thing as `Berry_flux_map()`, except that it computes all components of the Berry curvature (for a 3D model, like a Weyl semi-metal) and plots the transverse components as field lines, and the third component as a color map. Again we have a choice of the plane, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Mapping the Berry flux...\")\n",
    "I.Berry_flux_map(nk=50, plane='z', dir='z', k_perp=0.01, label=0, npoints=8, radius=None, file=None, plt_ax=None)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Mapping the Berry field lines on the xy plane, then on the xz plane...\")\n",
    "I.Berry_field_map(nk=80, nsides=4, plane='z', k_perp=0.01, label=0, file=None, plt_ax=None)\n",
    "I.Berry_field_map(nk=40, nsides=4, plane='y', k_perp=0.01, label=0, file=None, plt_ax=None)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### monopole()\n",
    "`monopole(k, a=0.01, nk=20, label=0, subdivide=False)`computes the flux of the Berry curvature through a closed cube of side `a` centered at wavevector `k` with `nk` wavevectors on the side of the cube. Again, the boolean option `subdivide` will subdivide the grid on each face of the cube to maintain a small gradient of the phase. To get a map of this function to the whole Brillouin zone, the function `monopole_map()` can be called."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Mapping the Berry monopoles at various value of kx...\")\n",
    "for kx in np.arange(0,1,0.025):\n",
    "    print('kx/pi = {:.2f} : monopole = {:.4f}'.format(kx, 1e-7+I.monopole([kx,0,0], a=0.04, nk=30, subdivide=True)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Mapping the Berry monopoles...\")\n",
    "I.monopole_map(k_perp=0.01)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "q_env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "9181c3fee36b2a40316c7a7a7190b5ebac7b971b62240dc3c988739896c48274"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
